# OpenHarmonyEnv

### 介绍

用于指导纯净系统下的傻瓜式、最快速搭建OpenHarmony环境。

### 系统安装

#### 1、获取镜像

建议找一个有启动盘的白嫖 0.0

win10镜像：[下载 Windows 10 (microsoft.com)](https://www.microsoft.com/zh-cn/software-download/windows10)

#### 2、安装指导

插入启动盘到电脑上，然后启动电脑，疯狂F9，进入BIOS模式，等待出现界面后选择首选项回车，再回车。

等待安装成功！

### 必备软件

#### 1、搜狗输入法

官网下载：[搜狗输入法 - 首页 (sogou.com)](https://pinyin.sogou.com/)

#### 2、VSCode（开发环境）

安装指导：https://www.cnblogs.com/csji/p/13558221.html

#### 3、typora（markdown）

安装指导：https://my.oschina.net/u/4399679/blog/3372590?hmsr=kaifa_aladdin

#### 4、everything（文件搜索）

安装包见工具集

#### 5、7z（压缩包工具）

安装包见工具集

#### 6、Snipaste（截图工具）

安装包见工具集

#### 7、notepad++（文本编辑器）

安装包见工具集

#### 8、Git（版本控制工具）

安装指导：https://www.cnblogs.com/xueweisuoyong/p/11914045.html

记得要配置username和email

```git
git config --global user.name "XXX"
git config --global user.email XXX@XXXXXX.com
```

#### 9、ubuntu子系统（目前不需要了，直接使用蓝区计算云）

安装指导：[Windows10开启Ubuntu子系统简易步骤 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/34133795)

映射ubuntu文件系统：

```wiki
1. 添加一个网络位置
2. 指定网站的位置
3. \\wsl$\Ubuntu-20.04
```

**注意：需要升级到WSL2，否则编译会很慢**

升级指导：[window10将wsl升级到wsl2](https://blog.csdn.net/qq_41048761/article/details/120860972)



### OpenHarmony代码下载以及编译环境搭建

#### 1、官方方法（蓝区计算云使用该方法）

```
// 代码下载
repo init -u https://gitee.com/openharmony/manifest.git -b master --no-repo-verify
repo sync -c -j32

// 编译构建L2的rk3568系统
repo forall -c 'git lfs pull'
bash build/prebuilts_download.sh
./build.sh --product-name rk3568 --ccache
```

[获取源码及Ubuntu编译环境准备 | OpenHarmony](https://www.openharmony.cn/pages/0001000202/)

#### 2、其他方法

##### 1、脚本下载

跑到ubuntu子系统中，clone下面这个仓库

```git
git clone git@gitee.com:landwind/openharmony_oneclick_env_init
```

##### 2、设置代码存储路径

```bash
cd openharmony_oneclick_env_init
```

进入该路径下后：

```bash
vim source.sh
```

将这行内容修改为自己准备存储OpenHarmony代码的路径：

```bash
work_dir='/home/***/workspace/open_harmony/master'
```

##### 3、执行脚本

```bash
bash init.sh;source /etc/profile
```

出现错误解决错误即可

如果出现gn下载失败，即：

![](https://s3.bmp.ovh/imgs/2021/12/871418bbd959ab4e.jpg)

请本地执行：

```shell
wget --no-check-certificate https://repo.huaweicloud.com/harmonyos/compiler/gn/1717/linux/gn-linux-x86-1717.tar.gz

然后把下载的gn文件移动到之前设置的workdir，如果出现权限不足，就手动chmod一下

重新执行bash init.sh;source /etc/profile
```

##### 4、常见错误

（1）python没有指定版本

```bash
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 100
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 150
```

（2）路径找不到sh文件

把对应sh里面的路径改成存放目标sh的绝对路径

### 结果检查

#### 1、全量代码

代码存放在自己设置的work_dir/OpenHarmony路径内

#### 2、编译结果

尝试编译L2或者L1，检查是否可以编译成功

##### L2

```bash
./build.sh --product-name rk3568
```

##### L1

```bash
hb set
hb build
```

### 烧录环境

#### 烧录工具下载（HiTool）

下载地址：[点此下载](http://www.hihope.org/download/download.aspx?mtt=33)

![示例](https://i.bmp.ovh/imgs/2022/01/baa87c2aeffa0503.png)

#### 设备驱动安装

串口驱动（USB-to-Serial Comm Port）：[HiHope官网](http://www.hihope.org/download/download.aspx?mtt=12)

USB烧录需要驱动HiUSBBurnDriver：[HiHope官网](http://www.hihope.org/download/download.aspx)

USB转串口驱动（CH341SER）：[HiHope官网](http://www.hihope.org/download/download.aspx?mtt=8)

