#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <getopt.h>
#include "hc_log.h"
#include "device_auth.h"
#include "securec.h"
#include "json_utils.h"
#include "string_util.h"
#include "hc_tlv_parser.h"
#include "broadcast_manager.h"
#include "common_defs.h"
#include <pthread.h>
#include "hc_condition.h"

#define SUCCESS 0
#define FAIL 1
#define PORT 10001
#define CONCURRENT_NUM 40

#define TEST_APP_NAME "com.huawei.security"

static bool isShowCommand = false;
static int g_isClient = 0;
static int g_reqId = 123;
static in_addr_t g_dstIp = 0;
static int clientfd = -1;
static char localSessionKey[128] = {0};
static int localSessionKeyLen = 0;
static int stopTask = 0;
static int isError = 0;
static int isProcessing = 0;
static bool g_isAuth = false;
static bool g_isConcurrent = false;
static const GroupAuthManager *g_gaInstance = NULL;
static const DeviceGroupManager *g_gmInstance = NULL;
static DeviceAuthCallback gmCallback = { NULL };
static DeviceAuthCallback gaCallback;
static DataChangeListener g_testListener = { NULL };
static HcCondition g_testCondition;

static int g_isAccepted = 1;
static int g_osAccountId = 0;
static int g_testNum = 0;
static char g_pinCode[128] = { 0 };
static char g_deviceId[128] = { 0 };
static char g_peerUdid[128] = "ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF01";
static char g_pkgName[128] = "com.huawei.security";

//char g_peerUdid[128] = "ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF01";
char g_hostUdid[128] = "ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00ABCDEF00";
char g_groupId[128] = "FF9B5A053D74CC8835DB98FAADBCE1A8E4C441637A3A170CB95D4DDD83319C49";

static bool OnTransmitNet(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    int32_t cnt;
    printf("-------------OnTransmit---------------\n");
    printf("|  RequestId: %-30lld  |\n", requestId);
    printf("|  SendData: %s  |\n", (char *)data);
    CJson *json = CreateJsonFromString((char *)data);
    int64_t tmpReqId = 0;
    if (GetInt64FromJson(json, FIELD_REQUEST_ID, &tmpReqId) != 0) {
        AddInt64StringToJson(json, FIELD_REQUEST_ID, requestId);
    }
    char *sendData = PackJsonToString(json);
    FreeJson(json);
    uint32_t sendDataLen = strlen(sendData) + 1;
    cnt = send(clientfd, (uint8_t *)sendData, sendDataLen, 0);
    FreeJsonString(sendData);
    usleep(100000);
    // sleep(1);
    printf("send size %d, process size %d\n", dataLen, cnt);
    return true;
}

static void OnSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
    printf("-------------OnSessionKeyReturned---------------\n");
    printf("|  RequestId: %-40lld  |\n", requestId);
    printf("|  SessionKey: %-36d  |\n", sessionKeyLen);
    memcpy(localSessionKey, sessionKey, sessionKeyLen);
    localSessionKeyLen = sessionKeyLen;
    printf("-------------OnSessionKeyReturned---------------\n");
}

static void OnFinish(int64_t requestId, int operationCode, const char *authReturn)
{
    printf("-------------OnFinish---------------\n");
    printf("|  RequestId: %-27lld  |\n", requestId);
    printf("|  OperationCode: %-23d  |\n", operationCode);
    printf("|  ReturnData: %-26s  |\n", authReturn);
    printf("-------------OnFinish---------------\n");
    if (operationCode == 0) {
        return;
    }
    if (operationCode == 4) {
        printf("delete member done, %s\n", authReturn);
        return;
    }
    CJson *dataJson = CreateJsonFromString(authReturn);
    const char* groupId = NULL;
    if (dataJson != NULL) {
        groupId = GetStringFromJson(dataJson, FIELD_GROUP_ID);
        if (groupId != NULL) {
            memcpy_s(g_groupId, strlen(groupId), groupId, strlen(groupId));
            g_groupId[strlen(groupId)] = 0;
        }
    }
    printf("g_groupId: %s\n", strlen(g_groupId) > 0 ? g_groupId : "empty\n");
}

static void OnError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    printf("-------------OnError---------------\n");
    printf("|  RequestId: %-27lld  |\n", requestId);
    printf("|  OperationCode: %-23d  |\n", operationCode);
    printf("|  ErrorCode: %-27d  |\n", errorCode);
    printf("-------------OnError---------------\n");
    if (operationCode == 0) {
        return;
    }
    isError = 1;
}

static char *OnBindRequestController(int64_t requestId, int operationCode, const char* reqParam)
{
    printf("-------------OnBindRequestController---------------\n");
    printf("|  RequestId: %-29lld  |\n", requestId);
    printf("|  OperationCode: %-25d  |\n", operationCode);
    printf("|  reqParam: %s  |\n", reqParam);
    CJson *json = CreateJson();
    if (g_isAccepted == 1)
    {
        AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_ACCEPTED);
    } else {
        AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_REJECTED);
    }
    
    AddIntToJson(json, FIELD_OS_ACCOUNT_ID, g_osAccountId);
    AddStringToJson(json, FIELD_PIN_CODE, g_pinCode);
    AddStringToJson(json, FIELD_DEVICE_ID, g_deviceId);
    char *returnDataStr = PackJsonToString(json);
    FreeJson(json);
    printf("|  returnDataStr: %s  |\n", returnDataStr);
    printf("-------------OnBindRequestController---------------\n");
    return returnDataStr;
}

static char *OnAuthRequestController(int64_t requestId, int operationCode, const char* reqParam)
{
    printf("-------------OnAuthRequestController---------------\n");
    printf("|  RequestId: %-29lld  |\n", requestId);
    printf("|  OperationCode: %-25d  |\n", operationCode);
    printf("|  reqParam: %s  |\n", reqParam);
    printf("-------------OnAuthRequestController---------------\n");
    CJson *json = CreateJson();
    if (g_isAccepted == 1)
    {
        AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_ACCEPTED);
    } else {
        AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_REJECTED);
    }
    AddIntToJson(json, FIELD_OS_ACCOUNT_ID, g_osAccountId);
    AddStringToJson(json, FIELD_PEER_CONN_DEVICE_ID, g_peerUdid);
    AddStringToJson(json, FIELD_SERVICE_PKG_NAME, g_pkgName);
    char *returnDataStr = PackJsonToString(json);
    FreeJson(json);
    return returnDataStr;
}

void LocalForceDelMember(void)
{
    CJson *delParam = CreateJson();
    AddStringToJson(delParam, FIELD_DEVICE_ID, g_peerUdid);
    AddStringToJson(delParam, FIELD_GROUP_ID, g_groupId);
    AddBoolToJson(delParam, FIELD_IS_FORCE_DELETE, true);
    char * delParamStr = PackJsonToString(delParam);
    FreeJson(delParam);
    g_gmInstance = GetGmInstance();
    g_gmInstance->deleteMemberFromGroup(0, 1234567890000001, TEST_APP_NAME, delParamStr);
    FreeJsonString(delParamStr);
}

void TestGetGroupInfo(void)
{
    CJson *returnData = CreateJson();
    AddStringToJson(returnData, FIELD_GROUP_OWNER, TEST_APP_NAME);
    char * queryParams = PackJsonToString(returnData);
    FreeJson(returnData);

    char* groupVec = NULL;
    uint32_t num = 0;
    g_gmInstance->getGroupInfo(0, TEST_APP_NAME, queryParams, &groupVec, &num);
    FreeJsonString(queryParams);
    printf("query result: %s, group num %d\n", groupVec, num);
    g_gmInstance->destroyInfo(&groupVec);
}

void TestGetGroupInfoById(void)
{
    char* groupVec = NULL;
    int32_t ret;
    char tmpCh;
    g_gmInstance->getGroupInfoById(0, TEST_APP_NAME, g_groupId, &groupVec);
    printf("query result: %s\n", groupVec);
    g_gmInstance->destroyInfo(&groupVec);

    g_gmInstance->getGroupInfoById(0, "aabb\n", g_groupId, &groupVec);
    printf("query result with unmatched app name: %s\n", groupVec);
    g_gmInstance->destroyInfo(&groupVec);

    tmpCh = g_groupId[15];
    g_groupId[15] = 'A';
    if (tmpCh == 'A') {
        g_groupId[15] = 'B';
    }
    g_gmInstance->getGroupInfoById(0, TEST_APP_NAME, g_groupId, &groupVec);
    printf("query result with unmatched app name: %s\n", groupVec);
    g_gmInstance->destroyInfo(&groupVec);
    g_groupId[15] = tmpCh;

    ret = g_gmInstance->getGroupInfoById(0, NULL, g_groupId, &groupVec);
    printf("query result without app name: %d\n", ret);
    ret = g_gmInstance->getGroupInfoById(0, TEST_APP_NAME, NULL, &groupVec);
    printf("query result without group id: %d\n", ret);
    ret = g_gmInstance->getGroupInfoById(0, TEST_APP_NAME, g_groupId, NULL);
    printf("query result without cache: %d\n", ret);
}

void TestGetTrustedDevices(void)
{
    char* devVec = NULL;
    uint32_t num = 0;
    int32_t ret;
    char tmpCh;
    g_gmInstance->getTrustedDevices(0, TEST_APP_NAME, g_groupId, &devVec, &num);
    printf("query result: %s, num %d\n", devVec, num);
    g_gmInstance->destroyInfo(&devVec);

    num = 0;
    g_gmInstance->getTrustedDevices(0, "aabb", g_groupId, &devVec, &num);
    printf("query result with unmatched app name: %s, num %d\n", devVec, num);
    g_gmInstance->destroyInfo(&devVec);

    tmpCh = g_groupId[15];
    g_groupId[15] = 'A';
    if (tmpCh == 'A') {
        g_groupId[15] = 'B';
    }
    num = 0;

    g_gmInstance->getTrustedDevices(0, TEST_APP_NAME, g_groupId, &devVec, &num);
    printf("query result with unmatched app name: %s, num %d\n", devVec, num);
    g_gmInstance->destroyInfo(&devVec);
    g_groupId[15] = tmpCh;

    ret = g_gmInstance->getTrustedDevices(0, NULL, g_groupId, &devVec, &num);
    printf("query result without app name: %d\n", ret);
    ret = g_gmInstance->getTrustedDevices(0, TEST_APP_NAME, NULL, &devVec, &num);
    printf("query result without group id: %d\n", ret);
    ret = g_gmInstance->getTrustedDevices(0, TEST_APP_NAME, g_groupId, NULL, &num);
    printf("query result without cache: %d\n", ret);
}

static int64_t GetReqIdFromData(const char *data)
{
    CJson *json = CreateJsonFromString(data);
    int64_t reqId = 123;
    GetInt64FromJson(json, FIELD_REQUEST_ID, &reqId);
    FreeJson(json);
    return reqId;
}

static void TestController3516InServer(void) {
    int authInit = 1;
    char buf[2048] = {0};

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1) {
        printf("socket failed\n");
        return;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(PORT);

    printf("start to bind socket!\n");
    int res = bind(sockfd, (struct sockaddr *)&addr, sizeof(addr));
    if (res == -1) {
        printf("bind failed\n");
        return;
    }

    printf("start to listen socket!\n");
    res = listen(sockfd, 100);
    if(res == -1) {
        printf("listen failed\n");
        close(sockfd);
        return;
    }
    printf("listen %d port, wait for new connection...\n", PORT);

    struct sockaddr_in fromaddr;
    socklen_t len = sizeof(fromaddr);
    while(1) {
        if(clientfd < 0) {
            printf("wait new connection...\n");
            clientfd = accept(sockfd, (struct sockaddr *)&fromaddr, &len);
            if(clientfd == -1) {
                printf("accept failed\n");
                continue;
            }
            printf("new client in:%s\n", inet_ntoa(fromaddr.sin_addr));
            isProcessing = 1;
            authInit = 1;
            while(clientfd > 0 && isProcessing) {
                (void)memset_s(buf, sizeof(buf), 0, sizeof(buf));
                int count = recv(clientfd, buf, sizeof(buf), 0);
                printf("read %d byte from client: %s\n", count, buf);
                if(count <= 0) {
                    close(clientfd);
                    clientfd = -1;
                    break;
                }
                if (strcmp("forceDelMember\n", buf) == 0) {
                    LocalForceDelMember();
                    sleep(1);
                    continue;
                }
                int64_t tmpReqId = GetReqIdFromData(buf);
                if(!g_isAuth) {
                    printf("fuzikun: bind!\n");
                    g_gmInstance->processData(tmpReqId, (uint8_t *)buf, (uint32_t)(count));
                } else {
                    printf("fuzikun: auth!\n");
                    g_gaInstance->processData(tmpReqId, (uint8_t *)buf, (uint32_t)(count), &gaCallback);
                }
            }
            sleep(2);
            if(stopTask == 1) {
                DestroyDeviceAuthService();
                stopTask = 0;
                break;
            }
            printf("test processing at ending...\n");
            sleep(1);
        } else {
            if(clientfd > 0) {
                close(clientfd);
                clientfd = -1;
            }
            sleep(2);
        }
    }

    if(clientfd > 0) {
        close(clientfd);
        clientfd = -1;
    }
    close(sockfd);
}

static int CreateGroup3516(void) {
    CJson *json = CreateJson();
    AddStringToJson(json, FIELD_GROUP_NAME, "P2PGroup");
    AddStringToJson(json, FIELD_DEVICE_ID, "client");
    AddIntToJson(json, FIELD_GROUP_TYPE, PEER_TO_PEER_GROUP);
    AddIntToJson(json, FIELD_GROUP_VISIBILITY, GROUP_VISIBILITY_PUBLIC);
    AddIntToJson(json, FIELD_USER_TYPE, DEVICE_TYPE_ACCESSORY);
    AddIntToJson(json, FIELD_EXPIRE_TIME, -1);
    char *createParamsStr = PackJsonToString(json);
    FreeJson(json);
    int res = g_gmInstance->createGroup(0, 123456,TEST_APP_NAME, createParamsStr);
    FreeJsonString(createParamsStr);
    return res;
}

static int Test_AddMember(void) {
    CJson *addParams = CreateJson();
    printf("group id: %s\n", g_groupId);
    AddStringToJson(addParams, FIELD_GROUP_ID, g_groupId);
    AddIntToJson(addParams, FIELD_GROUP_TYPE, PEER_TO_PEER_GROUP);
    AddStringToJson(addParams, FIELD_PIN_CODE, g_pinCode);
    AddBoolToJson(addParams, FIELD_IS_ADMIN, true);
    char *addParamsStr = PackJsonToString(addParams);
    FreeJson(addParams);
    int ret = -1;
    g_testCondition.wait(&g_testCondition);
    if (g_isConcurrent) {
        for (int i = 1; i <= CONCURRENT_NUM; i++) {
            char tmpAppId[256] = { 0 };
            sprintf(tmpAppId, "%s%d", TEST_APP_NAME, i);
            ret = g_gmInstance->addMemberToGroup(0, i, tmpAppId, addParamsStr);
        }
    } else {
        ret = g_gmInstance->addMemberToGroup(0, g_reqId, TEST_APP_NAME, addParamsStr);
    }
    // int ret = g_gmInstance->addMemberToGroup(g_reqId, TEST_APP_NAME, addParamsStr);
    FreeJsonString(addParamsStr);
    while(1) {};
    return ret;
}

static int Test_AddMemberToGroupServer(void) {

    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &g_osAccountId);

    printf("Please input pinCode: ");
    fflush(stdout);
    scanf("%s", g_pinCode);
    if (strcmp(g_pinCode, "null") == 0) {
        (void)memset_s(g_pinCode, sizeof(g_pinCode), 0, sizeof(g_pinCode));
    }

    printf("Please input isAccepted:0 is false,1 is true");
    fflush(stdout);
    scanf("%d", &g_isAccepted);

    printf("Please input deviceId: ");
    fflush(stdout);
    scanf("%s", g_deviceId);
    if (strcmp(g_deviceId, "null") == 0) {
        (void)memset_s(g_deviceId, sizeof(g_deviceId), 0, sizeof(g_deviceId));
    }
    
    TestController3516InServer();
    return 0;
}

static int Test_DeleteMemberFromGroupServer(void) {

    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &g_osAccountId);

    printf("Please input isAccepted:0 is false,1 is true");
    fflush(stdout);
    scanf("%d", &g_isAccepted);
    
    TestController3516InServer();
    return 0;
}

static int Test_AuthDevice(void)
{
    CJson *authParam = CreateJson();
    AddStringToJson(authParam, FIELD_PEER_AUTH_ID, "server");
    AddStringToJson(authParam, FIELD_SERVICE_PKG_NAME, TEST_APP_NAME);
    AddBoolToJson(authParam, FIELD_IS_CLIENT, true);
    char *authParamStr = PackJsonToString(authParam);
    FreeJson(authParam);
    int ret = -1;
    g_testCondition.wait(&g_testCondition);
    if (g_isConcurrent) {
        for (int i = 1; i <= CONCURRENT_NUM; i++) {
            ret = g_gaInstance->authDevice(0, i, authParamStr, &gaCallback);
        }
    } else {
        ret = g_gaInstance->authDevice(0, g_reqId, authParamStr, &gaCallback);
    }
    FreeJsonString(authParamStr);
    while(1) {};
    return ret;
}

static int TestOpenSocket(in_addr_t dstlp, short dstPort) {
    printf("connect ip: %s, dest port:%d\n", inet_ntoa(*((struct in_addr*)&dstlp)), dstPort);
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1) {
        printf("socket failed\n");
        return -1;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = dstlp;
    addr.sin_port = htons(dstPort);

    int res = connect(sockfd, (struct sockaddr *)&addr, sizeof(addr));
    if(res < 0) {
        printf("connect failed\n");
        close(sockfd);
        return -1;
    }
    return sockfd;
}

static void *ClientSocketFunc(void *args)
{
    printf("----------------------start to open socket!\n");
    clientfd = TestOpenSocket(g_dstIp, PORT);
    if (clientfd < 0) {
        clientfd = 0;
        return((void *)0);
    }
    printf("----------------------open socket success!\n");
    bool isFirst = true;
    char buf[2048] = {0};
    while (1) {
        (void)memset_s(buf, sizeof(buf), 0, sizeof(buf));
        printf("----------------------start to recv data!\n");
        if (isFirst) {
            g_testCondition.notify(&g_testCondition);
            isFirst = false;
        }
        printf("----------------------recv data!\n");
        int count = recv(clientfd, buf, sizeof(buf), 0);
        printf("----------------------recv data success!\n");
        printf("read %d bytes from peer: %s\n", count, buf);
        if (count <= 0) {
            close(clientfd);
            clientfd = -1;
            break;
        }
        if (strcmp("forceDelMember\n", buf) == 0) {
            LocalForceDelMember();
            sleep(1);
            continue;
        }
        int64_t tmpReqId = GetReqIdFromData(buf);
        if(!g_isAuth) {
            printf("----------------------fuzikun: bind!\n");
            g_gmInstance->processData(tmpReqId, (uint8_t *)buf, (uint32_t)(count));
        } else {
            printf("----------------------fuzikun: auth!\n");
            g_gaInstance->processData(tmpReqId, (uint8_t *)buf, (uint32_t)(count), &gaCallback);
        }
        printf("----------------------fuzikun: OK!\n");
    }
    return((void *)0);
}

void TestController3516InClient(void)
{
    int ret = CreateGroup3516();
    if (ret != 0) {
        printf("Create group failed\n");
        return;
    }
    pthread_t pid;
    (void)pthread_create(&pid, NULL, ClientSocketFunc, NULL);
    if (!g_isAuth) {
        ret = Test_AddMember();
    } else {
        ret = Test_AuthDevice();
    }
}

static int Test_AddMemberToGroupClient(void) 
{
    char groupId[128] = { 0 };
    printf("Please input groupId: ");
    fflush(stdout);
    scanf("%s", groupId);
    if (strcmp(groupId, "null") == 0) {
        (void)memset_s(groupId, sizeof(groupId), 0, sizeof(groupId));
    }

    int64_t requestId = 0;
    printf("Please input requestId: ");
    fflush(stdout);
    scanf("%lld", &requestId);

    int osAccountId = 0;
    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &osAccountId);

    char appId[128] = { 0 };
    printf("Please input appId: ");
    fflush(stdout);
    scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }

    int groupType = PEER_TO_PEER_GROUP;
    printf("Please input groupType: ");
    fflush(stdout);
    scanf("%d", &groupType);

    char pinCode[128] = { 0 };
    printf("Please input pinCode: ");
    fflush(stdout);
    scanf("%s", pinCode);
    if (strcmp(pinCode, "null") == 0) {
        (void)memset_s(pinCode, sizeof(pinCode), 0, sizeof(pinCode));
    }

    int isAdminInput = 1;
    printf("Please input isAdmin:0 is false,1 is true");
    fflush(stdout);
    scanf("%d", &isAdminInput);
    bool isAdmin = true;
    if (isAdminInput == 0)
    {
        isAdmin = false;
    }

    printf("Please input ip: ");
    char optarg[128] = { 0 };
    fflush(stdout);
    scanf("%s", optarg);
    if (strcmp(optarg, "null") == 0) {
        (void)memset_s(optarg, sizeof(optarg), 0, sizeof(optarg));
    }
    g_dstIp = inet_addr(optarg);
    
    pthread_t pid;
    (void)pthread_create(&pid, NULL, ClientSocketFunc, NULL);
    CJson *addParams = CreateJson();
    printf("group id: %s\n", groupId);
    AddStringToJson(addParams, FIELD_GROUP_ID, groupId);
    AddIntToJson(addParams, FIELD_GROUP_TYPE, groupType);
    AddStringToJson(addParams, FIELD_PIN_CODE, pinCode);
    AddBoolToJson(addParams, FIELD_IS_ADMIN, isAdmin);
    char *addParamsStr = PackJsonToString(addParams);
    FreeJson(addParams);
    int ret = -1;
    g_testCondition.wait(&g_testCondition);
    ret = g_gmInstance->addMemberToGroup(osAccountId, requestId, appId, addParamsStr);
    FreeJsonString(addParamsStr);
    while(1) {
        sleep(3);
        break;
    };
    return ret;
}

static int Test_DeleteMemberFromGroupClient(void) 
{
    char groupId[128] = { 0 };
    printf("Please input groupId: ");
    fflush(stdout);
    scanf("%s", groupId);
    if (strcmp(groupId, "null") == 0) {
        (void)memset_s(groupId, sizeof(groupId), 0, sizeof(groupId));
    }

    int64_t requestId = 0;
    printf("Please input requestId: ");
    fflush(stdout);
    scanf("%lld", &requestId);

    int osAccountId = 0;
    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &osAccountId);

    char appId[128] = { 0 };
    printf("Please input appId: ");
    fflush(stdout);
    scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }

    char peerAuthId[128] = { 0 };
    printf("Please input peerAuthId: ");
    fflush(stdout);
    scanf("%s", peerAuthId);
    if (strcmp(peerAuthId, "null") == 0) {
        (void)memset_s(peerAuthId, sizeof(peerAuthId), 0, sizeof(peerAuthId));
    }

    int isForceDeleteInput = 1;
    printf("Please input isForceDelete:0 is false,1 is true");
    fflush(stdout);
    scanf("%d", &isForceDeleteInput);
    bool isForceDelete = true;
    if (isForceDeleteInput == 0)
    {
        isForceDelete = false;
    }

    printf("Please input ip: ");
    char optarg[128] = { 0 };
    fflush(stdout);
    scanf("%s", optarg);
    if (strcmp(optarg, "null") == 0) {
        (void)memset_s(optarg, sizeof(optarg), 0, sizeof(optarg));
    }
    g_dstIp = inet_addr(optarg);
    
    pthread_t pid;
    (void)pthread_create(&pid, NULL, ClientSocketFunc, NULL);

    CJson *delParam = CreateJson();
    AddStringToJson(delParam, FIELD_DELETE_ID, peerAuthId);
    AddStringToJson(delParam, FIELD_GROUP_ID, groupId);
    AddBoolToJson(delParam, FIELD_IS_FORCE_DELETE, isForceDelete);
    char * delParamStr = PackJsonToString(delParam);
    FreeJson(delParam);

    int ret = -1;
    g_testCondition.wait(&g_testCondition);
    ret = g_gmInstance->deleteMemberFromGroup(osAccountId, requestId, appId, delParamStr);
    FreeJsonString(delParamStr);
    while(1) {
        sleep(3);
        break;
    };
    return ret;
}

static int Test_AuthDeviceClient(void) 
{
    g_isAuth = true;
    char authId[128] = { 0 };
    printf("Please input authId: ");
    fflush(stdout);
    scanf("%s", authId);
    if (strcmp(authId, "null") == 0) {
        (void)memset_s(authId, sizeof(authId), 0, sizeof(authId));
    }

    int64_t requestId = 0;
    printf("Please input requestId: ");
    fflush(stdout);
    scanf("%lld", &requestId);

    int osAccountId = 0;
    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &osAccountId);

    char pkgName[128] = { 0 };
    printf("Please input pkgName: ");
    fflush(stdout);
    scanf("%s", pkgName);
    if (strcmp(pkgName, "null") == 0) {
        (void)memset_s(pkgName, sizeof(pkgName), 0, sizeof(pkgName));
    }

    int isClientInput = 1;
    printf("Please input isClient:(0 is false,1 is true) ");
    fflush(stdout);
    scanf("%d", &isClientInput);
    bool isClient = true;
    if (isClientInput == 0)
    {
        isClient = false;
    }

    printf("Please input ip: ");
    char optarg[128] = { 0 };
    fflush(stdout);
    scanf("%s", optarg);
    if (strcmp(optarg, "null") == 0) {
        (void)memset_s(optarg, sizeof(optarg), 0, sizeof(optarg));
    }
    g_dstIp = inet_addr(optarg);
    
    pthread_t pid;
    (void)pthread_create(&pid, NULL, ClientSocketFunc, NULL);

    CJson *authParam = CreateJson();
    AddStringToJson(authParam, FIELD_PEER_AUTH_ID, authId);
    AddStringToJson(authParam, FIELD_SERVICE_PKG_NAME, pkgName);
    AddBoolToJson(authParam, FIELD_IS_CLIENT, isClient);
    char *authParamStr = PackJsonToString(authParam);
    FreeJson(authParam);
    int ret = -1;
    g_testCondition.wait(&g_testCondition);
    ret = g_gaInstance->authDevice(osAccountId, requestId, authParamStr, &gaCallback);
    FreeJsonString(authParamStr);
    while(1) {
        sleep(3);
        break;
    };
    return ret;
}

static int Test_AuthDeviceServer(void) 
{
    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &g_osAccountId);

    printf("Please input peerUdid: ");
    fflush(stdout);
    scanf("%s", g_peerUdid);
    if (strcmp(g_peerUdid, "null") == 0) {
        (void)memset_s(g_peerUdid, sizeof(g_peerUdid), 0, sizeof(g_peerUdid));
    }

    printf("Please input isAccepted:0 is false,1 is true");
    fflush(stdout);
    scanf("%d", &g_isAccepted);

    printf("Please input pkgName: ");
    fflush(stdout);
    scanf("%s", g_pkgName);
    if (strcmp(g_pkgName, "null") == 0) {
        (void)memset_s(g_pkgName, sizeof(g_pkgName), 0, sizeof(g_pkgName));
    }
    g_isAuth = true;
    TestController3516InServer();
    return 0;
}

void Test_OnGroupCreated(const char *groupInfo)
{
    if (groupInfo == NULL) {
        printf("not expected\n");
        return;
    }
    printf("-------------OnGroupCreated---------------\n");
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnGroupCreated---------------\n");
}

void Test_OnGroupDeleted(const char *groupInfo)
{
    if (groupInfo == NULL) {
        printf("not expected\n");
        return;
    }
    printf("-------------OnGroupDeleted---------------\n");
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnGroupDeleted---------------\n");
}

void Test_OnDeviceBound(const char *peerUdid, const char* groupInfo)
{
    printf("-------------OnDeviceBound---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnDeviceBound---------------\n");
}
void Test_OnDeviceUnBound(const char *peerUdid, const char* groupInfo)
{
    printf("-------------OnDeviceUnBound---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnDeviceUnBound---------------\n");
}
void Test_OnDeviceNotTrusted(const char *peerUdid)
{
    printf("-------------OnDeviceNotTrusted---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("-------------OnDeviceNotTrusted---------------\n");
}
void Test_OnLastGroupDeleted(const char *peerUdid, int groupType)
{
    printf("-------------OnLastGroupDeleted---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("GroupType:\n%d\n", groupType);
    printf("-------------OnLastGroupDeleted---------------\n");
}
void Test_OnTrustedDeviceNumChanged(int curTrustedDeviceNum)
{
    printf("-------------OnTrustedDeviceNumChanged---------------\n");
    printf("CurTrustedDeviceNum:\n%d\n", curTrustedDeviceNum);
    printf("-------------OnTrustedDeviceNumChanged---------------\n");
}

void devauth_test_bind(void (*startHook)())
{
    (void)memcpy_s(g_pinCode, 128, "123456", 6);
    (void)memcpy_s(g_deviceId, 128, "server", 6);
    if (g_isClient) {
        TestController3516InClient();
    } else {
        TestController3516InServer();
    }
}

static struct option longOpts[] = {
    { "group", required_argument, NULL, 'g' },
    { "peer", required_argument, NULL, 'p' },
    { "auth", no_argument, NULL, 'a' },
    { "client", no_argument, NULL, 0 },
    { "dip", required_argument, NULL, 1 },
    { "host", required_argument, NULL, 2 },
    { "concurrent", no_argument, NULL, 3 },
    { "reqId", required_argument, NULL, 'r' },
    { "command", no_argument, NULL, 'c' },
    { 0, 0, 0, 0 }
};

static int32_t ParseTestParams(int argc, char **argv)
{
    int ch;
    int ret = 0;
    int idx = 0;
    const char *shortOpts = "qg:p:";

    while ((ch = getopt_long(argc, argv, shortOpts, longOpts, &idx)) >= 0) {
        switch (ch) {
            case 'c':
                isShowCommand = true;
                break;
            case 'g':
                if (optarg == NULL || strlen(optarg) == 0 || strlen(optarg) >= 128) {
                    g_groupId[0] = 0;
                } else {
                    memcpy_s(g_groupId, strlen(optarg), optarg, strlen(optarg));
                    g_groupId[strlen(optarg)] = 0;
                }
                break;
            case 'p':
                if (optarg == NULL || strlen(optarg) == 0 || strlen(optarg) >= 128) {
                    break;
                }
                memcpy_s(g_peerUdid, strlen(optarg), optarg, strlen(optarg));
                g_peerUdid[strlen(optarg)] = 0;
                break;
            case 'r':
                if (optarg != NULL) {
                    g_reqId = atoi(optarg);
                }
                break;
            case 'a':
                printf("fuzikun: auth!\n");
                g_isAuth = true;
                break;
            case 0:
                g_isClient = 1;
                break;
            case 1:
                if (optarg == NULL || strlen(optarg) == 0) {
                    ret = -1;
                    break;
                }
                g_dstIp = inet_addr(optarg);
                printf("test dest ip: %s, hex: 0x%08x\n", optarg, g_dstIp);
                break;
            case 2:
                if (optarg == NULL || strlen(optarg) == 0 || strlen(optarg) >= 128) {
                    break;
                }
                memcpy_s(g_hostUdid, strlen(optarg), optarg, strlen(optarg));
                g_hostUdid[strlen(optarg)] = 0;
                break;
            case 3:
                g_isConcurrent = true;
                break;
            default:
                break;
        }
    }
    return ret;
}

static int InitEnv()
{
    printf("start to init env.\n");
    InitDeviceAuthService();
    g_gmInstance = GetGmInstance();
    g_gaInstance = GetGaInstance();
    if (g_gmInstance == NULL || g_gaInstance == NULL) {
        DestroyDeviceAuthService();
        printf("init module instance failed\n");
        return -1;
    }
    g_testListener.onGroupCreated = Test_OnGroupCreated;
    g_testListener.onGroupDeleted = Test_OnGroupDeleted;
    g_testListener.onDeviceBound = Test_OnDeviceBound;
    g_testListener.onDeviceUnBound = Test_OnDeviceUnBound;
    g_testListener.onDeviceNotTrusted = Test_OnDeviceNotTrusted;
    g_testListener.onLastGroupDeleted = Test_OnLastGroupDeleted;
    g_testListener.onTrustedDeviceNumChanged = Test_OnTrustedDeviceNumChanged;
    g_gmInstance->regDataChangeListener(TEST_APP_NAME, &g_testListener);

    gmCallback.onError = OnError;
    gmCallback.onFinish = OnFinish;
    gmCallback.onSessionKeyReturned = OnSessionKeyReturned;
    gmCallback.onTransmit = OnTransmitNet;
    gmCallback.onRequest = OnBindRequestController;
    g_gmInstance->regCallback(TEST_APP_NAME, &gmCallback);
    if (g_isConcurrent && !g_isAuth) {
        for (int i = 1; i <= CONCURRENT_NUM; i++) {
            char tmpAppId[256] = { 0 };
            sprintf(tmpAppId, "%s%d", TEST_APP_NAME, i);
            g_gmInstance->regCallback(tmpAppId, &gmCallback);;
            printf("test regCallback count:%d \n", i);
        }
    }

    gaCallback.onError = OnError;
    gaCallback.onFinish = OnFinish;
    gaCallback.onSessionKeyReturned = OnSessionKeyReturned;
    gaCallback.onTransmit = OnTransmitNet;
    gaCallback.onRequest = OnAuthRequestController;
    printf("init env success.\n");
    return 0;
}

static void Test_GetPkInfoList(void)
{
    CJson *json = CreateJson();
    int input = -1;
    printf("isSelfPk(0=false, 1=true): ");
    fflush(stdout);
    scanf("%d", &input);
    if (input == 0) {
        AddBoolToJson(json, FIELD_IS_SELF_PK, false);
    } else if (input == 1) {
        AddBoolToJson(json, FIELD_IS_SELF_PK, true);
    } else {
        printf("Invalid params!\n");
        FreeJson(json);
        return;
    }
    char queryUdid[129] = { 0 };
    printf("udid: ");
    fflush(stdout);
    scanf_s("%s", queryUdid, 129);
    AddStringToJson(json, FIELD_UDID, queryUdid);
    char *queryParamsStr = PackJsonToString(json);
    FreeJson(json);
    char *queryResult = NULL;
    uint32_t resultNum = 0;
    // int res = g_gmInstance->getPkInfoList(TEST_APP_NAME, queryParamsStr, &queryResult, &resultNum);
    int res = HC_ERROR;
    FreeJsonString(queryParamsStr);
    if (res == 0) {
        printf("[RESULT][GetPkInfoList]: SUCCESS\n");
        printf("returnInfoList: %s\n", queryResult);
        printf("returnInfoNum: %u\n", resultNum);
        g_gmInstance->destroyInfo(&queryResult);
        return;
    }
    printf("[RESULT][GetPkInfoList]: FAIL\nret: %d\n", res);
    return;
}

static void Test_GetGroupInfoById(void)
{
	int osAccountId = 0;
	printf("Please input osAccountId: ");
	fflush(stdout);
	scanf("%d", &osAccountId);
	char appId[128] = { 0 };
	printf("Please input appId: ");
	fflush(stdout);
	scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    char groupId[128] = { 0 };
    printf("Please input groupId: ");
	fflush(stdout);
	scanf("%s", groupId);
    if (strcmp(groupId, "null") == 0) {
        (void)memset_s(groupId, sizeof(groupId), 0, sizeof(groupId));
    }
    char *queryResult = NULL;
    int res = g_gmInstance->getGroupInfoById(osAccountId, appId, groupId, &queryResult);
    if (res == 0) {
        printf("[RESULT][GetGroupInfoById]: SUCCESS\n");
        printf("ReturnGroupInfo: %s\n", queryResult);
        g_gmInstance->destroyInfo(&queryResult);
        return;
    }
    printf("[RESULT][GetGroupInfoById]: FAIL\nret: %d\n", res);
    return;
}

static void Test_CheckAccessToGroup(void)
{
	int osAccountId = 0;
	printf("Please input osAccountId: ");
	fflush(stdout);
	scanf("%d", &osAccountId);
	char appId[128] = { 0 };
	printf("Please input appId: ");
	fflush(stdout);
	scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    char groupId[128] = { 0 };
    printf("Please input groupId: ");
	fflush(stdout);
	scanf("%s", groupId);
    if (strcmp(groupId, "null") == 0) {
        (void)memset_s(groupId, sizeof(groupId), 0, sizeof(groupId));
    }
    int res = g_gmInstance->checkAccessToGroup(osAccountId, appId, groupId);
    if (res == 0) {
        printf("[RESULT][CheckAccessToGroup]: SUCCESS\n");
        return;
    }
    printf("[RESULT][CheckAccessToGroup]: FAIL\nret: %d\n", res);
}

static void Test_IsDeviceInGroup(void) {
	int osAccountId = 0;
	printf("Please input osAccountId: ");
	fflush(stdout);
	scanf("%d", &osAccountId);
	char appId[128] = { 0 };
	printf("Please input appId: ");
	fflush(stdout);
	scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    char groupId[128] = { 0 };
    printf("Please input groupId: ");
	fflush(stdout);
	scanf("%s", groupId);
    if (strcmp(groupId, "null") == 0) {
        (void)memset_s(groupId, sizeof(groupId), 0, sizeof(groupId));
    }
	char deviceId[128] = { 0 };
    printf("Please input deviceId: ");
	fflush(stdout);
	scanf("%s", deviceId);
    if (strcmp(deviceId, "null") == 0) {
        (void)memset_s(deviceId, sizeof(deviceId), 0, sizeof(deviceId));
    }
	bool res = g_gmInstance->isDeviceInGroup(osAccountId, appId, groupId, deviceId);
	if (res) {
        printf("[RESULT][IsDeviceInGroup]: TRUE\n");
        return;
    }
    printf("[RESULT][IsDeviceInGroup]: FALSE\n");
    return;
}

static void Test_GetGroupManagers(void) {
	printf("not support\n");
}

static void Test_GetGroupFriends(void) {
	printf("not support\n");
}

static void Test_GetGroupInfo(void)
{
	int osAccountId = 0;
	printf("Please input osAccountId: ");
	fflush(stdout);
	scanf("%d", &osAccountId);
	char appId[128] = { 0 };
	printf("Please input appId: ");
	fflush(stdout);
	scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    CJson *queryParams = CreateJson();
    char groupId[128] = { 0 };
    printf("Please input groupId: ");
    fflush(stdout);
    scanf("%s", groupId);
    if (strcmp(groupId, "null") != 0) {
        AddStringToJson(queryParams, FIELD_GROUP_ID, groupId);
    }
    char groupName[128] = { 0 };
    printf("Please input groupName: ");
    fflush(stdout);
    scanf("%s", groupName);
    if (strcmp(groupName, "null") != 0) {
        AddStringToJson(queryParams, FIELD_GROUP_NAME, groupName);
    }
    char groupOwner[128] = { 0 };
    printf("Please input groupOwner: ");
    fflush(stdout);
    scanf("%s", groupOwner);
    if (strcmp(groupOwner, "null") != 0) {
        AddStringToJson(queryParams, FIELD_GROUP_OWNER, groupOwner);
    }
    AddIntToJson(queryParams, FIELD_GROUP_TYPE, PEER_TO_PEER_GROUP);
    char *queryParamsStr = PackJsonToString(queryParams);
    FreeJson(queryParams);
    char *queryResult = NULL;
    uint32_t resultNum = 0;
    int res = g_gmInstance->getGroupInfo(osAccountId, appId, queryParamsStr, &queryResult, &resultNum);
    FreeJsonString(queryParamsStr);
    if (res == 0) {
        printf("[RESULT][GetGroupInfo]: SUCCESS\n");
        printf("ReturnGroupInfos: %s\n", queryResult);
        printf("Group count: %d\n", resultNum);
        g_gmInstance->destroyInfo(&queryResult);
        return;
    }
    printf("[RESULT][GetGroupInfo]: FAIL\nret: %d\n", res);
    return;
}

static void Test_GetJoinedGroups(void)
{
	int osAccountId = 0;
	printf("Please input osAccountId: ");
	fflush(stdout);
	scanf("%d", &osAccountId);
	char appId[128] = { 0 };
	printf("Please input appId: ");
	fflush(stdout);
	scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
	int32_t groupType = 0;
	printf("Identical account group: 1\nPeer to peer group: 256\nIdentical account associated group: 1026\nAcross account authorized group: 1282\nPlease input group type: ");
	fflush(stdout);
	scanf("%d", &groupType);
    char *queryResult = NULL;
    uint32_t resultNum = 0;
    int res = g_gmInstance->getJoinedGroups(osAccountId, appId, groupType, &queryResult, &resultNum);
    if (res == 0) {
        printf("[RESULT][GetJoinedGroups]: SUCCESS\n");
        printf("ReturnGroupInfos: %s\n", queryResult);
        printf("Group count: %d\n", resultNum);
        g_gmInstance->destroyInfo(&queryResult);
        return;
    }
    printf("[RESULT][GetJoinedGroups]: FAIL\nret: %d\n", res);
    return;
}

static void Test_GetRelatedGroups(void)
{
	int osAccountId = 0;
	printf("Please input osAccountId: ");
	fflush(stdout);
	scanf("%d", &osAccountId);
	char appId[128] = { 0 };
	printf("Please input appId: ");
	fflush(stdout);
	scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
	char deviceId[128] = { 0 };
	printf("Please input deviceId: ");
	fflush(stdout);
	scanf("%s", deviceId);
    if (strcmp(deviceId, "null") == 0) {
        (void)memset_s(deviceId, sizeof(deviceId), 0, sizeof(deviceId));
    }
    char *queryResult = NULL;
    uint32_t resultNum = 0;
    int res = g_gmInstance->getRelatedGroups(osAccountId, appId, deviceId, &queryResult, &resultNum);
    if (res == 0) {
        printf("[RESULT][GetRelatedGroups]: SUCCESS\n");
        printf("ReturnGroupInfos: %s\n", queryResult);
        printf("Group count: %d\n", resultNum);
        g_gmInstance->destroyInfo(&queryResult);
        return;
    }
    printf("[RESULT][GetRelatedGroups]: FAIL\nret: %d\n", res);
    return;
}

static void Test_GetDeviceInfoById(void)
{
	int osAccountId = 0;
	printf("Please input osAccountId: ");
	fflush(stdout);
	scanf("%d", &osAccountId);
	char appId[128] = { 0 };
	printf("Please input appId: ");
	fflush(stdout);
	scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
	char deviceId[128] = { 0 };
	printf("Please input deviceId: ");
	fflush(stdout);
	scanf("%s", deviceId);
    if (strcmp(deviceId, "null") == 0) {
        (void)memset_s(deviceId, sizeof(deviceId), 0, sizeof(deviceId));
    }
	char groupId[128] = { 0 };
	printf("Please input groupId: ");
	fflush(stdout);
	scanf("%s", groupId);
    if (strcmp(groupId, "null") == 0) {
        (void)memset_s(groupId, sizeof(groupId), 0, sizeof(groupId));
    }
    char *returnDeviceInfo = NULL;
    int res = g_gmInstance->getDeviceInfoById(osAccountId, appId, deviceId, groupId, &returnDeviceInfo);
    if (res == 0) {
        printf("[RESULT][GetDeviceInfoById]: SUCCESS\n");
        printf("Return device info: %s\n", returnDeviceInfo);
        g_gmInstance->destroyInfo(&returnDeviceInfo);
        return;
    }
    printf("[RESULT][GetDeviceInfoById]: FAIL\nret: %d\n", res);
    return;
}

static void Test_GetTrustedDevices(void)
{
	int osAccountId = 0;
	printf("Please input osAccountId: ");
	fflush(stdout);
	scanf("%d", &osAccountId);
	char appId[128] = { 0 };
	printf("Please input appId: ");
	fflush(stdout);
	scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
	char groupId[128] = { 0 };
	printf("Please input groupId: ");
	fflush(stdout);
	scanf("%s", groupId);
    if (strcmp(groupId, "null") == 0) {
        (void)memset_s(groupId, sizeof(groupId), 0, sizeof(groupId));
    }
    char *returnDevices = NULL;
	uint32_t resultNum = 0;
    int res = g_gmInstance->getTrustedDevices(osAccountId, appId, groupId, &returnDevices, &resultNum);
    if (res == 0) {
        printf("[RESULT][GetTrustedDevices]: SUCCESS\n");
        printf("Return device infos: %s\n", returnDevices);
		printf("Devices count: %d\n", resultNum);
        g_gmInstance->destroyInfo(&returnDevices);
        return;
    }
    printf("[RESULT][GetTrustedDevices]: FAIL\nret: %d\n", res);
    return;
}

static void Test_DeleteGroup(void)
{
    printf("Delete Group\n");
    int64_t requestId = 0;
    printf("Please input requestId: ");
    fflush(stdout);
    scanf("%lld", &requestId);
    int osAccountId = 0;
    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &osAccountId);
    char appId[128] = { 0 };
    printf("Please input appId: ");
    fflush(stdout);
    scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    CJson *json = CreateJson();
    char groupId[128] = { 0 };
    printf("Please input groupId: ");
    fflush(stdout);
    scanf("%s", groupId);
    if (strcmp(groupId, "null") != 0) {
        AddStringToJson(json, FIELD_GROUP_ID, groupId);
    }
    char *disBandParamsStr = PackJsonToString(json);
    FreeJson(json);
    int res = g_gmInstance->deleteGroup(osAccountId, requestId, appId, disBandParamsStr);
    FreeJsonString(disBandParamsStr);
    if (res == 0) {
        printf("Delete Group success\n");
    } else {
        printf("Delete Group fail\nret: %d\n", res);
    }
}

static void Test_CreateGroup(void)
{
    printf("start to Create Group\n");
    int64_t requestId = 0;
    printf("Please input requestId: ");
    fflush(stdout);
    scanf("%lld", &requestId);
    int osAccountId = 0;
    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &osAccountId);
    char appId[128] = { 0 };
    printf("Please input appId: ");
    fflush(stdout);
    scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    int groupType = PEER_TO_PEER_GROUP;
    printf("Please input groupType: ");
    fflush(stdout);
    scanf("%d", &groupType);
    int groupVisibility = GROUP_VISIBILITY_PUBLIC;
    printf("Please input groupVisibility: ");
    fflush(stdout);
    scanf("%d", &groupVisibility);
    CJson *json = CreateJson();
    char groupName[128] = { 0 };
    printf("Please input groupName: ");
    fflush(stdout);
    scanf("%s", groupName);
    if (strcmp(groupName, "null") != 0) {
        AddStringToJson(json, FIELD_GROUP_NAME, groupName);
    }
    char deviceId[128] = { 0 };
    printf("Please input deviceId: ");
    fflush(stdout);
    scanf("%s", deviceId);
    if (strcmp(deviceId, "null") != 0) {
        AddStringToJson(json, FIELD_DEVICE_ID, deviceId);
    }
    AddIntToJson(json, FIELD_GROUP_TYPE, groupType);
    AddIntToJson(json, FIELD_GROUP_VISIBILITY, groupVisibility);
    AddIntToJson(json, FIELD_USER_TYPE, DEVICE_TYPE_ACCESSORY);
    AddIntToJson(json, FIELD_EXPIRE_TIME, -1);
    char *createParamsStr = PackJsonToString(json);
    FreeJson(json);
    int32_t res = g_gmInstance->createGroup(osAccountId, requestId, appId, createParamsStr);
    FreeJsonString(createParamsStr);
	if (res == 0) {
		printf("Create Group success\n");
	} else {
		printf("Create Group fail:\nret: %d\n", res);
	}
}

static void Test_InitSerice(void)
{
    printf("Init Serice start\n");
    int ret = InitDeviceAuthService();
    printf("ret: %d\n", ret);
}

static void Test_DestroySerice(void)
{
    printf("Destroy Serice start\n");
    DestroyDeviceAuthService();
    printf("Destroy Serice end\n");
}

static void Test_RegCallback(void)
{
    printf("RegCallback start\n");
    char appId[128] = { 0 };
    printf("Please input appId: ");
    fflush(stdout);
    scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    int ret = g_gmInstance->regCallback(appId, &gmCallback);
    printf("ret: %d\n", ret);
}

static void Test_UnregCallback(void)
{
    printf("UnregCallback start\n");
    char appId[128] = { 0 };
    printf("Please input appId: ");
    fflush(stdout);
    scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    int ret = g_gmInstance->unRegCallback(appId);
    printf("ret: %d\n", ret);
}

static void Test_RegListener(void)
{
    printf("RegListener start\n");
    char appId[128] = { 0 };
    printf("Please input appId: ");
    fflush(stdout);
    scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    int ret = g_gmInstance->regDataChangeListener(appId, &g_testListener);
    printf("ret: %d\n", ret);
}

static void Test_UnregListener(void)
{
    printf("UnregListener start\n");
    char appId[128] = { 0 };
    printf("Please input appId: ");
    fflush(stdout);
    scanf("%s", appId);
    if (strcmp(appId, "null") == 0) {
        (void)memset_s(appId, sizeof(appId), 0, sizeof(appId));
    }
    int ret = g_gmInstance->unRegDataChangeListener(appId);
    printf("ret: %d\n", ret);
}

static void CreateAndQueryInner(int32_t osAccountId, int32_t testNum)
{
    int createSuccessNum = 0;
    int querySuccessNum = 0;
    int deleteSuccessNum = 0;
    for (int i = 0; i < testNum; i++)
    {
        CJson *json = CreateJson();
        AddStringToJson(json, FIELD_GROUP_NAME, "Test_DFXSingleThreadCreateAndQuery");
        AddStringToJson(json, FIELD_DEVICE_ID, "client");
        AddIntToJson(json, FIELD_GROUP_TYPE, PEER_TO_PEER_GROUP);
        AddIntToJson(json, FIELD_GROUP_VISIBILITY, GROUP_VISIBILITY_PUBLIC);
        AddIntToJson(json, FIELD_USER_TYPE, DEVICE_TYPE_ACCESSORY);
        AddIntToJson(json, FIELD_EXPIRE_TIME, -1);
        char *createParamsStr = PackJsonToString(json);
        FreeJson(json);
        int res = g_gmInstance->createGroup(osAccountId, 123456, TEST_APP_NAME, createParamsStr);
        FreeJsonString(createParamsStr);
        printf("[RESULT][createGroup] ret: %d\n", res);
        if (res == 0)
        {
            createSuccessNum++;
        }
        
        CJson *queryParams = CreateJson();
        AddStringToJson(queryParams, FIELD_GROUP_ID, "3F5AE005954C05E45E16AA040B97FB8EB2CF04951DBB7465444DD27FE012BA1F");
        AddIntToJson(queryParams, FIELD_GROUP_TYPE, PEER_TO_PEER_GROUP);
        char *queryParamsStr = PackJsonToString(queryParams);
        FreeJson(queryParams);
        char *queryResult = NULL;
        uint32_t resultNum = 0;
        res = g_gmInstance->getGroupInfo(osAccountId, TEST_APP_NAME, queryParamsStr, &queryResult, &resultNum);
        FreeJsonString(queryParamsStr);
        if (res == 0) {
            printf("[RESULT][GetGroupInfo]: SUCCESS\n");
            printf("ReturnGroupInfos: %s\n", queryResult);
            printf("Group count: %d\n", resultNum);
            g_gmInstance->destroyInfo(&queryResult);
        }
        printf("[RESULT][GetGroupInfo]: ret: %d\n", res);
        if (res == 0)
        {
            querySuccessNum++;
        }

        CJson *delJson = CreateJson();
        AddStringToJson(delJson, FIELD_GROUP_ID, "3F5AE005954C05E45E16AA040B97FB8EB2CF04951DBB7465444DD27FE012BA1F");
        char *disBandParamsStr = PackJsonToString(delJson);
        FreeJson(delJson);
        res = g_gmInstance->deleteGroup(osAccountId, 123456, TEST_APP_NAME, disBandParamsStr);
        FreeJsonString(disBandParamsStr);
        printf("[RESULT][deleteGroup]: ret: %d\n", res);
        if (res == 0)
        {
            deleteSuccessNum++;
        }
    }
    sleep(5);
    printf("[RESULT][total]: num: %d\n", testNum);
    printf("[RESULT][create Success]: num: %d\n", createSuccessNum);
    printf("[RESULT][query Success]: num: %d\n", querySuccessNum);
    printf("[RESULT][delete Success]: num: %d\n", deleteSuccessNum);
}

static void Test_DFXSingleThreadCreateAndQuery(void)
{
    int osAccountId = 0;
    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &osAccountId);

    int testNum = 0;
    printf("Please input test num: ");
    fflush(stdout);
    scanf("%d", &testNum);
    CreateAndQueryInner(osAccountId, testNum);
}



static void *mulThreadCreateAndQueryInner(void *args)
{
    CreateAndQueryInner(g_osAccountId, g_testNum);
    return((void *)0);
}

static void Test_DFXMulThreadCreateAndQuery(void)
{
    printf("Please input osAccountId: ");
    fflush(stdout);
    scanf("%d", &g_osAccountId);

    printf("Please input test num: ");
    fflush(stdout);
    scanf("%d", &g_testNum);

    int pidNum = 0;
    printf("Please input pidNum: ");
    fflush(stdout);
    scanf("%d", &pidNum);

    pthread_t thread[pidNum];
 
    for(int t = 0; t < pidNum; t++)
    {
        printf("Creating thread %d\n", t);
        (void)pthread_create(&thread[t], NULL, mulThreadCreateAndQueryInner, NULL);
    }
}

static void ShowCommand(void)
{
    setvbuf(stdout, NULL, _IONBF, 0);
    int command = 0;
    while (command != -1) {
        printf("\n");
        printf("|--------------------------------|\n");
        printf("| %-30s |\n", "01-CreateGroup");
        printf("| %-30s |\n", "02-DeleteGroup");
        printf("| %-30s |\n", "03-GetPkInfoList");
        printf("| %-30s |\n", "04-GetGroupInfoById");
        printf("| %-30s |\n", "05-GetGroupInfo");
		printf("| %-30s |\n", "06-GetGroupManagers");
		printf("| %-30s |\n", "07-GetGroupFriends");
		printf("| %-30s |\n", "08-GetJoinedGroups");
		printf("| %-30s |\n", "09-GetRelatedGroups");
		printf("| %-30s |\n", "10-GetDeviceInfoById");
		printf("| %-30s |\n", "11-GetTrustedDevices");
		printf("| %-30s |\n", "12-IsDeviceInGroup");
		printf("| %-30s |\n", "13-CheckAccessToGroup");
        printf("| %-30s |\n", "14-InitService");
        printf("| %-30s |\n", "15-DestroyService");
        printf("| %-30s |\n", "16-RegisterCallback");
        printf("| %-30s |\n", "17-UnRegisterCallback");
        printf("| %-30s |\n", "18-AddMemberToGroupClient");
        printf("| %-30s |\n", "19-AddMemberToGroupServer");
        printf("| %-30s |\n", "20-AuthDeviceClient");
        printf("| %-30s |\n", "21-AuthDeviceServer");
		printf("| %-30s |\n", "22-RegListener");
        printf("| %-30s |\n", "23-UnregListener");
		printf("| %-30s |\n", "24-DeleteMemberFromGroupClient");
        printf("| %-30s |\n", "25-DeleteMemberFromGroupServer");
        printf("| %-30s |\n", "26-DFXSingleThreadCreateAndQuery");
        printf("| %-30s |\n", "27-DFXMulleThreadCreateAndQuery");
        printf("| %-30s |\n", "-1-Quit");
        printf("|--------------------------------|\n");
        printf("Please input your command: ");
        fflush(stdout);
        scanf("%d", &command);
        switch(command) {
            case 1:
                Test_CreateGroup();
                break;
            case 2:
                Test_DeleteGroup();
                break;
            case 3:
                Test_GetPkInfoList();
                break;
            case 4:
                Test_GetGroupInfoById();
                break;
            case 5:
                Test_GetGroupInfo();
                break;
			case 6:
				Test_GetGroupManagers();
				break;
			case 7:
				Test_GetGroupFriends();
				break;
			case 8:
				Test_GetJoinedGroups();
				break;
			case 9:
				Test_GetRelatedGroups();
				break;
			case 10:
				Test_GetDeviceInfoById();
				break;
			case 11:
				Test_GetTrustedDevices();
				break;
			case 12:
				Test_IsDeviceInGroup();
				break;
			case 13:
				Test_CheckAccessToGroup();
				break;
            case 14:
				Test_InitSerice();
				break;
            case 15:
				Test_DestroySerice();
				break;
            case 16:
				Test_RegCallback();
				break;
            case 17:
				Test_UnregCallback();
				break;
            case 18:
				Test_AddMemberToGroupClient();
				break;
            case 19:
				Test_AddMemberToGroupServer();
				break;
            case 20:
				Test_AuthDeviceClient();
				break;
            case 21:
				Test_AuthDeviceServer();
				break;
            case 22:
                Test_RegListener();
                break;
            case 23:
                Test_UnregListener();
                break;
            case 24:
                Test_DeleteMemberFromGroupClient();
                break;
            case 25:
                Test_DeleteMemberFromGroupServer();
                break;
            case 26:
                Test_DFXSingleThreadCreateAndQuery();
                break;
            case 27:
                Test_DFXMulThreadCreateAndQuery();
                break;
            case -1:
                printf("leaving ...\n");
                break;
            default:
                printf("Invalid command!\n");
                break;
        }
        sleep(1);
    }
}

int main(int argc, char **argv)
{
    char *phone_udid = "C0D297A0979EA998BC1B25761E57A64833E2D9F524CF0B735B8C967FCCF8EBC8";
    memcpy_s(g_peerUdid, strlen(phone_udid), phone_udid, strlen(phone_udid));
    g_peerUdid[strlen(phone_udid)] = 0;
    if (ParseTestParams(argc, argv)) {
        return 0;
    }
    printf("test host udid %s\n", g_hostUdid);
    printf("test peer udid %s\n", g_peerUdid);
    printf("test specified group id: %s\n", g_groupId[0] != 0 ? g_groupId : "null\n");
    InitHcCond(&g_testCondition, NULL);
    if (InitEnv() != 0) {
        printf("init fail.\n");
        return 0;
    }
    if (isShowCommand) {
        ShowCommand();
    } else {
        devauth_test_bind(NULL);
    }
    DestroyDeviceAuthService();
    DestroyHcCond(&g_testCondition);
    return 0;
}