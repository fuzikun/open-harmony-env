qemu_option=""
qemu_net_id=""

help_info=$(cat <<-END
Usage: qemu-run [OPTION]...
Run a OHOS image in qemu according to the options.

    Options:
    -c   --copy image         copy virtual machine images, incluing: zImage-dtb, ramdist.img, system.img, vendor.img, userdata.img
    -e,  --exec image_path    build images path, including: zImage-dtb, ramdisk.img, system.img, vendor.img, userdata.img
    -g,  --gdb                enable gdb for kernel
    -n,  --net id             auto configure network, support opening multiple devices
    -h,  --help               print help info
END
)

function echo_help()
{
    echo "${help_info}"
    exit 0
}

function qemu_option_add()
{
    qemu_option+=" "
    qemu_option+="$1"
}

function parameter_verification()
{
    if [ $1 -eq 0 ] || [ x"$(echo $2 | cut -c 1)" = x"-" ]; then
        echo_help
    fi
}

function qemu_network()
{
    qemu_option_add "-netdev bridge,id=net0 -device virtio-net-device,netdev=net0,mac=${qemu_net_id}2:22:33:44:55:66"
}

function copy_img()
{
    cp $elf_file/userdata.img $elf_file/userdata${qemu_net_id}.img
    cp $elf_file/vendor.img $elf_file/vendor${qemu_net_id}.img
    cp $elf_file/system.img $elf_file/system${qemu_net_id}.img
    cp $elf_file/updater.img $elf_file/updater${qemu_net_id}.img
    echo "copy image success"
    echo "virutal machine number: $qemu_net_id"
    echo "save path: $elf_file/"
    exit 0
}

elf_file="out/qemu-arm-linux/packages/phone/images"

while [ $# -ne 0 ]
do
    case $1 in
        "-e")
            shift
            parameter_verification $# $1
            elf_file="$1"
            ;;
        "-n")
            shift
            parameter_verification $# $1
            qemu_net_id=$1
            qemu_network
            ;;
        "-c")
            shift
	    qemu_net_id=$1
	    copy_img
	    ;;
        "-g")
            qemu_option_add "-s -S"
            ;;
        "-h")
            echo_help
            ;;
    esac

    shift
done

qemu_option_add "-drive if=none,file=$elf_file/userdata${qemu_net_id}.img,format=raw,id=userdata,index=3 -device virtio-blk-device,drive=userdata"
qemu_option_add "-drive if=none,file=$elf_file/vendor${qemu_net_id}.img,format=raw,id=vendor,index=2 -device virtio-blk-device,drive=vendor"
qemu_option_add "-drive if=none,file=$elf_file/system${qemu_net_id}.img,format=raw,id=system,index=1 -device virtio-blk-device,drive=system"
qemu_option_add "-drive if=none,file=$elf_file/updater${qemu_net_id}.img,format=raw,id=updater,index=0 -device virtio-blk-device,drive=updater"
qemu_option_add "-kernel $elf_file/zImage-dtb -initrd $elf_file/../../../ramdisk.img"

echo "虚拟机准备启动..."

sudo qemu-system-arm -M virt -cpu cortex-a7 -smp 4 -m 1024 -nographic \
    ${qemu_option} \
    -append "console=ttyAMA0,115200 init=/bin/init hardware=qemu.arm.linux default_boot_device=a003e00.virtio_mmio root=/dev/ram0 rw"